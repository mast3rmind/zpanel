#!/bin/bash

# Include helper functions.
if [ "$(type -t run)" != "function" ]; then
    . ./scripts/helper.sh
fi

header_msg

echo "Starting ZPANEL web stack installation..."
echo "Please ensure that you're on a fresh install!"

echo "deb [trusted=yes] https://apt.fury.io/caddy/ /" | sudo tee -a /etc/apt/sources.list.d/caddy-fury.list
sudo apt update
sudo apt-get install caddy -y

STATUS="$(sudo systemctl is-active caddy.service)"
if [ "${STATUS}" = "active" ]; then
    echo "Installed caddy web server ....."
else 
    echo "Caddy wasn't found running.... so exiting installer"
    exit 1  
fi

# Add ondrej repo and install PHP

sudo apt-get install software-properties-common
sudo add-apt-repository ppa:ondrej/php

sudo apt install php-cli php-fpm php-mysql php-curl curl

sudo apt install mariadb-server
sudo systemctl enable mariadb
sudo mysql_secure_installation

read -p "Please enter your public domain name: " DOMAIN
sudo sed -i 's/example.com/'$DOMAIN'/' Caddyfile.example
sudo mkdir -p /var/www/$DOMAIN/html
sudo mkdir /var/log/caddy
sudo chown -R caddy:caddy /var/log/caddy
sudo cp Caddyfile.example Caddyfile
sudo cp Caddyfile /etc/caddy/

sudo sed -i 's/user = www-data/user = caddy/' /etc/php/8.0/fpm/pool.d/www.conf
sudo sed -i 's/group = www-data/group = caddy/' /etc/php/8.0/fpm/pool.d/www.conf
sudo sed -i 's/listen.owner = www-data/listen.owner = caddy/' /etc/php/8.0/fpm/pool.d/www.conf
sudo sed -i 's/listen.group = www-data/listen.group = caddy/' /etc/php/8.0/fpm/pool.d/www.conf

sudo systemctl restart php8.0-fpm

STATUS="$(sudo systemctl is-active php8.0-fpm.service)"
if [ "${STATUS}" = "active" ]; then
    echo "Installed PHP8.0-FPM....."
else 
    echo "PHP wasn't found running.... so exiting installer"  
    exit 1  
fi

sudo systemctl restart caddy

footer_msg
