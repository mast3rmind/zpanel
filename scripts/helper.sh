#!/bin/bash

# Set default colors
RED=31
GREEN=32
YELLOW=33
BLUE=94

function begin_color() {
    color="${1}"
    echo -e -n "\e[${color}m"
}

function end_color() {
    echo -e -n "\e[0m"
}

function echo_color() {
    color="${1}"
    shift
    begin_color "${color}"
    echo "$@"
    end_color
}

function status() {
    echo_color "${BLUE}" "$@"
}

function warning() {
    echo_color "${YELLOW}" "$@"
}

function success() {
    echo_color "${GREEN}" -n "Success: " >&2
    echo "$@" >&2
}

function info() {
    echo_color "${YELLOW}" -n "Info: " >&2
    echo "$@" >&2
}

function header_msg() {
    clear
    
    status "
#==========================================================================#
    ___       ___       ___       ___       ___       ___   
   /\  \     /\  \     /\  \     /\__\     /\  \     /\__\  
  _\:\  \   /::\  \   /::\  \   /:| _|_   /::\  \   /:/  /  
 /::::\__\ /::\:\__\ /::\:\__\ /::|/\__\ /::\:\__\ /:/__/   
 \::;;/__/ \/\::/  / \/\::/  / \/|::/  / \:\:\/  / \:\  \   
  \:\__\      \/__/    /:/  /    |:/  /   \:\/  /   \:\__\  
   \/__/               \/__/     \/__/     \/__/     \/__/ 
#==========================================================================#
#          Welcome to the ZPANEL installer for Debian/Ubuntu server        #
#==========================================================================#
#     Bash scripts to install LCMP (Linux, Caddy, MariaDB (MySQL), PHP)    #
#                                                                          #
#==========================================================================#
    "
}

# Footer credit message.
function footer_msg() {
    cat <<- _EOF_
#==========================================================================#
#        Thank you for installing ZPANEL web stack installer               #
#        Found any bugs/errors, or suggestions? please repor them          #
#                                                                          #
#          (c) 2021 | ZX Softworks                                         #
#==========================================================================#
_EOF_
}
